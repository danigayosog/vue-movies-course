# vue-movies-course

## Project setup
```
npm install
```

### Setup moviedb api key
Fill VUE_APP_MOVIEDB_API_KEY in .env or .env.local to fetch data from https://developers.themoviedb.org/3


### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
